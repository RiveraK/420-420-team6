from django.apps import AppConfig

#Application's name
class ItemsConfig(AppConfig):
    name = 'items'
