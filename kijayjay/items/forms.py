# -*- coding: utf-8 -*-
"""
Created on Tue May 18 16:43:15 2021

@author: keren
"""

from django import forms

from postgresql_db.models import Item

#Form to add an item to sell, it contains all details about the item
class SellItemForm(forms.ModelForm):
    price = forms.DecimalField(max_digits=10, decimal_places=2, label_suffix=(' ($)'))
    class Meta:
        model = Item
        fields = [
            'title',
            'description',
            'price',
            'images'
            ]

#Form to add an item to sell, it contains all details about the item
class BoughtItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = [
            'title',
            'description',
            'price',
            'images'
            ]