# -*- coding: utf-8 -*-
"""
Created on Tue May 11 14:29:23 2021

@author: keren
"""

from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views
from user import views as user_views

app_name='items'

#Urls to each of my item related views
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('sell_item', views.itemSell_view, name='sell_item' ),
    path('item/<int:pk>/', views.GetItemDetail.as_view(),name='item_details'),
    path('item/like/<int:pk>/', views.LikeView.as_view(), name='like_items'),
    path('items_liked/', views.LikedItemsView.as_view(), name='liked_items'),
    path('my_items/', views.MyItemsView.as_view(), name='my_items'),
    path('item/buy_item/<int:pk>/', views.BuyItemView.as_view(), name='buy_item'),
    path('item/delete/<int:pk>/', views.ItemDeleteView.as_view(),name='item_delete'),
    path('user/<int:pk>/', user_views.ProfileView.as_view(), name='profile'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)