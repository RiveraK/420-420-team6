from django.shortcuts import render, get_object_or_404, redirect
from django.views import generic
from postgresql_db.models import Item, Like, Profile
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.urls import reverse_lazy, reverse
from .forms import SellItemForm, BoughtItemForm
from django.contrib.auth.decorators import login_required
from urllib.parse import urlencode
from django.contrib import messages

#IndexView is the homepage for the Kijayjay site
class IndexView(generic.ListView):
        template_name = 'items/index.html'
        paginate_by = 5
        model = Item
        ordering = ('-pk')     
        #Return filtered items depending of select search query.
        def get_queryset(self):
            query = self.request.GET.get('search')
            filter_by = self.request.GET.get('filter_by')
            sort_by = self.request.GET.get('sort_by')
            page = self.request.GET.get('page')
            if query:
                if filter_by=='title':
                    if 'ASC' in sort_by:
                        object_list = Item.objects.filter(title__icontains=query).order_by(sort_by.replace('ASC',''))
                    elif 'DESC' in sort_by:
                        object_list = Item.objects.filter(title__icontains=query).order_by('-'+sort_by.replace('DESC',''))
                elif filter_by=='price':
                    if 'ASC' in sort_by:
                        object_list = Item.objects.filter(price__icontains=query).order_by(sort_by.replace('ASC',''))
                    elif 'DESC' in sort_by:
                            object_list = Item.objects.filter(price__icontains=query).order_by('-'+sort_by.replace('DESC',''))
                return object_list
            else:
                return Item.objects.all()
        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            gt = self.request.GET.copy()
            if 'page' in gt:
                del gt['page']
            context['params'] = urlencode(gt)
            context['title'] = 'Items Listed'
            return context



#View for the user to add the item it desires to sell   
@login_required
def itemSell_view(request):
    if request.method == 'POST':
        form = SellItemForm(request.POST, request.FILES)
        if form.is_valid():
            obj = form.save(commit = False)
            obj.user = request.user;
            obj.save()
            form.save()
            return redirect('items:index')
    else:
        form = SellItemForm()    
    return render(request, 'items/sell_item.html', {'form':form})

#It allows the user to view details of item from the homepage
class GetItemDetail(LoginRequiredMixin,generic.DetailView):
    template_name = 'items/item_details.html'
    model = Item
    extra_context = {'title':'Item details'}
    #permission_required = 'items.view_items'
    
#It allows the user to view details of item from the homepage
class BuyItemView(LoginRequiredMixin, generic.DetailView):
    '''template_name = 'items/buy_item.html'
    model = Item
    fields = ['title','description', 'images', 'price', 'location']
    permission_required = 'items.view_items' '''
    def post(self, request, *args, **kwargs):
        profile = Profile.objects.filter(user =request.user).get()
        item_bought = get_object_or_404(Item,id=request.POST.get('item_id'))
        
        if (profile.money_available < item_bought.price):
             messages.success(request,'Insufficient money')
             return render(request, 'items/index.html')
        else:
            profile.money_available = profile.money_available - item_bought.price
            profile.save()
            seller_profile =  Profile.objects.filter(user = item_bought.user).get()
            seller_profile.money_available = seller_profile.money_available + item_bought.price
            seller_profile.save()
            item_bought.delete()
            return redirect('items:profile')

#View that allows registered users to like an item
class LikeView(LoginRequiredMixin, generic.View):
    #Stores the user's like to the database for the item they liked.
    def post(self, request, *args, **kwargs):
        like = Like ()
        like.item = get_object_or_404(Item, id=request.POST.get('item_id'))
        profile = Profile.objects.filter(user =request.user).get()
        item =  get_object_or_404(Item, id=request.POST.get('item_id'))
        item.likes = item.likes + 1
        item.save()
        like.user =  User.objects.filter(id= profile.user.id).get()
        like.save()
        return redirect('items:index')
    
@login_required
def boughtItem_view(request):
    if request.method == 'POST':
        form = BoughtItemForm(request.POST, request.FILES)
        if form.is_valid():
            obj = form.save(commit = False)
            obj.user = request.user;
            obj.save()
            form.save()
            return redirect('items:index')
    else:
        form = ()    
    return render(request, 'items/sell_item.html', {'form':form})

#View to delete an item
class ItemDeleteView(LoginRequiredMixin,generic.DeleteView):
    model = Item
    def post(self, request, *args, **kwargs):
        todelete = Item ()
        todelete.item = get_object_or_404(Item, id=request.POST.get('item_id'))
        todelete.delete()
        return redirect('items:index')
    
#Displays the items that an user liked.
class LikedItemsView(generic.ListView):
    template_name = 'items/liked_items.html'
    paginate_by = 8
    model = Like
    extra_context = {'title':'Liked Items'}
    ordering = ('-pk')
    #Retrieves all the items the user liked.
    def get_queryset(self):
        user_obj= User.objects.filter(id=self.request.user.id).get()
        object_list = Like.objects.filter(user=user_obj) 
        return object_list

# The view that lets a registered user edit their posted for sell item.
class ItemEditView(LoginRequiredMixin,PermissionRequiredMixin, generic.UpdateView):
    model = Item
    fields = ['title','description', 'images', 'price', 'location']
    extra_context = {'edit':'Edit item'}
    permission_required = 'postgresql_db.change_items'
    success_url = reverse_lazy('index')

class MyItemsView(generic.ListView):
    template_name = 'items/my_items.html'
    paginate_by = 5
    model = Item
    ordering = ('-pk') 
    def get_queryset(self):
         user_items= Item.objects.filter(user_id=self.request.user.id)
         return user_items