from django.apps import AppConfig

class PostgresqlDbConfig(AppConfig):
    name = 'postgresql_db'

