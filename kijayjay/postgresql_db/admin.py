from django.contrib import admin
from .models import Item, Like, Profile, Comment
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.models import LogEntry
'''class ItemInline(admin.StackedInline):
    model = Item


class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['first_name', 'last_name', 'birth_date', 'email', 'username', 'password', 'status', 'money_available']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ItemInline]'''

# Register your models here.
admin.site.register(Item)
admin.site.register(Like)
admin.site.register(Profile)
admin.site.register(Comment)

