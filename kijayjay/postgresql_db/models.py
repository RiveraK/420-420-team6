from django.db import models
import datetime
from django.urls import reverse
from django.contrib.auth.models import User

from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
#Profile model, it references the User model provided by django and it provides extra fields that the django model 
#doesn't provide
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=9, default ='Customer')
    money_available = models.DecimalField(default=100, max_digits=10, decimal_places=2)
    picture = models.ImageField(upload_to="item_pictures/",default = "default_user_pic.png")
#method used to create a link between the profile and the user when a visitor registers
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
#used for profile changes
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

#Item model which contains all the information of item being sold and bought, it has a user's id(User)
#as foreign key
class Item(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=150)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    likes = models.IntegerField(default=0)
    images = models.ImageField(upload_to="item_pictures/", default = "default_item_pic.png")
    pub_date = models.DateField(default=datetime.date.today)
    def get_absolute_url(self):
        return reverse('item_details', kwargs={'pk':self.id})

#An entry is made in the Like table linking and user and item
class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)    

#Comments made on item
class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    comment = models.TextField(blank=False, help_text='Enter a comment')
    comment_date = models.DateTimeField(default=datetime.date.today)
    