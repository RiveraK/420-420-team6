from .forms import UserForm, ProfileForm, ProfileUpdateForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.shortcuts import redirect
from django.shortcuts import render
from django.views import generic
from django.utils import timezone
from postgresql_db.models import Profile
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import Group, User
from django.contrib.auth.forms import AuthenticationForm

#Sign up authentication method
def signup(request):
        user_form = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            profile = profile_form.save(commit = False)
            profile.user_id = user.id
            username = user_form.cleaned_data.get('username')
            password = user_form.cleaned_data.get('password')
            #user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('items:index')
        else:
            messages.error(request, 'Please correct the error below.')
        return render(request, 'user/signup.html', {'user_form': user_form,'profile_form': profile_form})

def login_view(request):
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('items:index')
        else:
            return render(request, 'user/login.html', {'form': form}) 

#Update profile method
@login_required
def update_profile(request, pk):
    user = Profile.objects.filter(user_id=request.user.id).get()
    if request.method == 'POST':
        p_form = ProfileUpdateForm(request.POST,request.FILES,instance=request.user.profile)
       
        if p_form.is_valid():
            p_form.picture = request.user.profile.picture
            p_form.save()
            user.username = request.user.username
            user.email = request.user.email
            
            user.save()
            messages.success(request,'Your Profile has been updated!')
            return redirect('items:index')
    else:
       # initial_p = {'picture': request.user.profile.picture,'username': request.user.username}
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context={'p_form': p_form, 'title':'Update Profile'}
    return render(request, 'user/profile_update.html',context )

class ProfileView(LoginRequiredMixin, generic.DetailView):
    template_name = 'user/profile.html'
    model = Profile
    fields = ['birth_date','money_available', 'picture']
    #permission_required = 'items.view_customer'
