
from django.urls import path

from . import views
from items import views as iview
from django.contrib.auth import views as auth_views

app_name='user'

#My urls to each of my views
urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('login/',auth_views.LoginView.as_view(template_name ='user/login.html'), name='login'),
    path('', iview.IndexView.as_view(), name='index'),
    path('user/<int:pk>/', views.ProfileView.as_view(), name='profile'),
    path('user/update/<int:pk>/', views.update_profile, name='update_profile'),
]