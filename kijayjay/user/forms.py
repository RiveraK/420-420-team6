# -*- coding: utf-8 -*-
"""
Created on Wed May 12 17:51:58 2021

@author: keren
"""
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from postgresql_db.models import Profile

#The sign up form is created using as a base some of the fields from the User Model and the Profile model
#So it uses both UserForm and ProfileForm (for those extra fields)
class UserForm(forms.ModelForm):
    first_name = forms.CharField(label='First name:', max_length=30, required=True, help_text='Required')
    last_name = forms.CharField(label='Last name',max_length=30, required=True, help_text='Required')
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password')
    def save(self, commit=True):
    # Save the provided password in hashed format
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

#Form for profile data
#This form has a name field and picture field.
class ProfileForm(forms.ModelForm):
    birth_date = forms.DateField(label='Date of birth:', help_text='Enter a date with any of the following formats: Y-m-d, m/d/y'
                                 , widget=forms.widgets.DateTimeInput)
    class Meta:
        model = Profile
        fields = ('picture', 'birth_date')
        
# This form has a username field and an picture field.
class ProfileUpdateForm(UserChangeForm):
    birth_date = forms.DateField(label='Date of birth:', help_text='Enter a date with any of the following formats: Y-m-d, m/d/y'
                                 , widget=forms.widgets.DateTimeInput, required=False)
    email = forms.EmailField(label='Email:', max_length=254,required=False, help_text='Enter a valid email address')
    username = forms.CharField(label='Username', max_length=70, required=False)
    password = forms.CharField(widget=forms.PasswordInput(), required=False)
    class Meta:
        model=Profile
        fields=['picture','birth_date']



        

        